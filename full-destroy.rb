require 'chef/provisioning'
   with_driver 'aws'
   environment  = 'offline-installation-test'

   with_chef_environment environment
   with_chef_server(
      "https://ip-172-31-64-85.eu-west-1.compute.internal/organizations/test-organisation",
      client_name: Chef::Config[:node_name],
      signing_key_filename: Chef::Config[:client_key]
)



### CONFIGURE YOUR CLUSTER BELOW ###

c8ycore_count = 3
ontoplb_count = 1
ssagent_count = 1
mongodb_count = 3
kubernetes_master_count   = 3
kubernetes_worker_count   = 3
### END OF CLUSTER CONFIGURATION ###


        1.upto(mongodb_count) do |i|
            machine "#{environment}_mongodb_#{i}" do
              action :destroy
            end
        end

        1.upto(c8ycore_count) do |i|
            machine "#{environment}_core_#{i}" do
              action :destroy
            end
        end

        1.upto(ontoplb_count) do |i|
            machine "#{environment}_lb_#{i}" do
              action :destroy
            end
        end

        1.upto(ssagent_count) do |i|
            machine "#{environment}_agent" do
              action :destroy
            end
        end

        1.upto(kubernetes_master_count) do |i|
            machine "#{environment}_master_#{i}" do
              action :destroy
            end
        end

        1.upto(kubernetes_worker_count) do |i|
            machine "#{environment}_worker_#{i}" do
              action :destroy
            end
        end

file "/tmp/.ps-#{environment}.steps" do
    action :delete
end

