#!/bin/bash

package_version=1009.0.0
#only lovercase are allowed 
organisation='test-organisation'
organisation_description='test'
client='cli'
client_password=gf6EXMi9
chef_server_hostname=$(hostname -f)
environment_name=offline-installation-test
environment_description="Offline installation test environment"
mongodb_password=init-root
mongodb_initPassword=test123
mkdir -p /root/$organisation/
cp -r cumulocity-chef /root/$organisation/
cd /root/$organisation/cumulocity-chef
###install-chef-server###
yum install -y http://$chef_server_hostname:8081/repository/Cumulocity-Yum/Base/chef-server-core-12.19.26-1.el7.x86_64.rpm
chef-server-ctl reconfigure
###client-key###
chef-server-ctl user-create $client Admin Chef12 C8Y chef12@cumulocity.com $client_password --filename /root/$organisation/cumulocity-chef/.chef/access_certs/$organisation-$client.pem
###client-secred###
chef-server-ctl org-create $organisation $organisation_description --filename /root/$organisation/cumulocity-chef/.chef/access_certs/$organisation-validator.pem 
##add client to admin group###
chef-server-ctl org-user-add $organisation $client --admin
###knife.rb###
echo 'current_dir = File.dirname(__FILE__)


log_level                :info
log_location             STDOUT
node_name                "'$client'"
client_key               "#{current_dir}/access_certs/'$organisation'-'$client'.pem"
validation_client_name   "'$organisation'-validator"
validation_key           "#{current_dir}/access_certs/'$organisation'-validator.pem"
chef_server_url          "https://'$chef_server_hostname'/organizations/'$organisation'"
syntax_check_cache_path  "#{ENV["'HOME'"]}/.chef/syntaxcache"
cookbook_path            ["#{current_dir}/../cookbooks"]
' > .chef/knife.rb 

export PATH=/opt/chef/embedded/bin:$PATH
gem sources --remove https://rubygems.org/
gem sources --add http://$chef_server_hostname:8081/repository/Cumulocity-Rubygems/
grep /opt/chef/embedded/bin:$PATH  /root/.bashrc  || /opt/chef/embedded/bin:$PATH >>  /root/.bashrc &&  source .bashrc

gem install chef-vault
knife ssl fetch 

#Set up vault
echo '
{
  "mongodb.password":"'$mongodb_password'",
  "mongodb.initPassword":"'$mongodb_password'"
}' >.chef/secrets/$environment_name.core.json

knife vault create secrets $environment_name.core -A $client -M client -S 'name:*' -J .chef/secrets/$environment_name.core.json

###data-bag###
knife data bag create users_cumulocity
knife data bag create certs
knife data bag from file certs data_bags/certs/certificate.json

knife role from file roles/*
knife cookbook upload --all

###default docker### 

echo '{
    "extPort":"30002",
    "useMongoDriver":true,
    "dockerUser":"admin",
    "dockerPassword":"admin123",
    "dockercreds":"admin:$2y$05$8jo8Ix/DHjbHlz0Nk/lJIOjl1Lye5n97ovNwtPa12xyw5KOshLI0i",
    "dockersecrt":"eyJrdWJlLXJlZ2lzdHJ5LXBlcnNpc3RlbnQtc2VjdXJlLmN1bXVsb2NpdHktc3RhZ2luZzAwNy1ub25wcm9kLnN2Yy5jbHVzdGVyLmxvY2FsOjUwMDAiOnsidXNlcm5hbWUiOiJhZG1pbiIsInBhc3N3b3JkIjoibXlQYXNzd29yZCIsImVtYWlsIjoiYWRtaW5Ac3RhZ2luZy5jOHkuaW8iLCJhdXRoIjoiWVdSdGFXNDZiWGxRWVhOemQyOXlaQT09In19"
}' > .chef/secrets/$environment_name.docker.json 


knife vault create secrets $environment_name.docker -A $client -M client -S '*' -J .chef/secrets/$environment_name.docker.json



###set environment###
echo '{     
     "name": "'$environment_name'",
     "description": "'$environment_description'",
     "cookbook_versions": {
      "cumulocity": "'$package_version'",
      "cumulocity-kubernetes": "'$package_version'",
      "cumulocity-ssagents": "'$package_version'"
    },
    "json_class": "Chef::Environment",
    "chef_type": "environment",
    "default_attributes": {
  
    },
    "override_attributes": {
      "offline-installation": true,
      "c8yrelease": "'$package_version'",
      "cumulocity-omnirepo": {
        "nexus": {
          "server":"'$chef_server_hostname'"
        }
      },
      "useVaults": false,
      "domainname": "acme.com",
      "environment": {
        "address": "management.devel-jenkins.c8y.io"
      },
      "swapfilesize": 512,
      "yum": {
        "repositories": {
          "cumulocity-testing": {
            "enabled": "0"
          },
          "cumulocity": {
            "url": "https://cumulocity:ACceP=m+2m@yum.cumulocity.com/centos/7/cumulocity/x86_64/"
          }
        }
      },
      "java": {
        "jdk_version": "8"
      },
      "cumulocity-kubernetes": {
        "dockerUser": "admin",
        "dockerPassword": "admin123",
	"docker-registry-image": "cumulocity/registry:2.7.3",
        "extPort": 30001,
        "useMongoDriver": true,
        "deployK8S4env": "'$environment_name'",
        "attachedEnvs": [
          "'$environment_name'"
        ],
        "token": "1e3145.2ff901841c78af1d",
        "images-connString": "https://K8Simages:K8S^imAgEs5000%@resources.cumulocity.com/kubernetes-images",
        "images-version": "'$package_version'",
        "images2install": [
          "cep",
          "cep-small",
          "device-simulator",
          "smartrule"
        ],
        "monitoring": {
          "enabled": true,
          "dashboard-only": true,
          "components": {
            "heapster": true
          }
        },
      "haproxy_version": "2.2.2-1.el7.c8y"
    },
      "cumulocity-karaf": {
        "CUMULOCITY_LICENCE_KEY": "1456d5050bb58ae50ea99079c193573bfeb769322fdec1f8e2cb599aa5026478428bd927410ba042885167daade61567a6444baeec7ce4fee6b4cec0fd0fe84d",
        "version": "'$package_version'-1",
        "ssa-version": "'$package_version'-1",
        "memory_left_for_system": "2048",
        "notification": true,
        "cep-server-enabled": true,
        "CUMULOCITY_LICENCE_DIR": "/etc/cumulocity/",
        "management-access": [
          "0.0.0.0/0"
        ],
        "logging": {
          "microservice_client": {
            "enabled": true
          },
          "microservice_proxy": {
            "enabled": false
          }
        },
        "karaf": {
          "memory": {
            "xms": "1024M"
          }
        }
      },
      "cumulocity-cep": {
        "version": "9.12.2-1",
        "package_name": "cep",
        "properties": {
          "esperha.storage": "/mnt/esperha-storage"
        }
      },
      "cumulocity-mongo": {
        "sharedkey-content": "1y7LbnZkJvDgtUOHN+8L++DAABlWdLO6kA+GXR23vl5QlslmqlB6goKQmDzgeMdA\nGC38ZcPLejm2Mnvk3TF7QHlhW1OvQZFOk600/Z9qbkzIjfQLNU4RIOdWq7pTq70w\nsyIbBXAZ+ZS2AUQnObxRiToIeDxakzjuiTQbwfYz7Z2bA/hJMrKNdI//IeRl93gt\nMAV5f07l5WQQ8OcKjqYlga1J2izcVmcbd6Q0PCtp38MrmBe3iEn34FpiAgDVZp06\ncuNJDUwr2YF90KWLs53g85vfhybNchxISXMSJBFApId8cuVeZ2oRKf7HjcyrsRR6\nUxk/74MMKvsXdxG2e2pfgTywyZ5Ndk5pGKXj6TZ5QY4Qw2QHryVPyRT90xogdDtg\nA4A8iSWRBgnrtJP+qvlfBSCpdN0EqmHqGuWcqzkc4sjpO9ubQdqvBFni9X0A6mxE\nWwGH2tk6uWQU4+OPfoQkVgUCFgepFWuzWHj9TA71sn0hmDLnBZDUh3yKcEz++qKy\nchfOPrnhSPpvZI0762F5LdIp7cuAwMC4wEYSSloawzqBnCpvQ0BsFAyprhZhFDdV\nUP67nmp/q5oaXgdr3TJOTGkRgcPXRSuf4zV4nKdMdyy7HM9o24LGXiJ40b3CZGhm\nyG0tRoTRNTd6hgFQWYp8hT4EK++kf60boGhUSPxvlkbERZ/mx4kPGY1fYWkRN8Y8\nbZXDnwu+A3kqCwSTJ6tjzrtqlQ51z5rJWl14eIo2Ienfym1tquoPNMeksQroivRB\n1ZXlA3v68+nHy2HljMsLUjt8oxho3HhN1RcDXazf4b39n5nZS4wOxjvPvqSrX4bw\n/Hwh8wL2+IDfOLl1yAO6isrEXApJSTiXFt5fSbaPW6T7hCiCkNPzdS+FYLArozNE\nYrzvmkbcHfMqqTCdWDSOWV7pRqvUARRFi0CvjWh85zmt4LG7IY/GBKJvmSAfFX1O\n5OCavvQrRbnH/m1xW7NHXbeWH80K",
        "mongodb.initUser": "init-root",
        "mongodb.initPassword": "'$mongodb_password'",
        "version": "4.2",
        "wiredtiger-cache": 4
      },
      "cumulocity-GUI": {
        "connString": "https://C8YWebApps:dkieW^s99l0@resources.cumulocity.com/targets/cumulocity/e153c733d590",
        "version": "1004.7.0"
      },
      "cumulocity-ssagents": {
        "useTags": true,
        "lwm2m-agent": {
          "host_fwUpdate": "34.251.8.163",
          "leshan_cluster_tenant": "management",
          "leshan_cluster_tenant_username": "lwm2m_user",
          "leshan_cluster_tenant_password": "passw0rd_a"
        }
      },
      "cumulocity-core": {
        "properties": {
          "system.connectivity.microservice.url": "http://${JWIRELESS-AGENT-SERVER}:8092/",
          "default.tenant.microservices": "device-simulator, smartrule, cep",
          "device-simulator.microservice.url": "http://localhost:8181/service/device-simulator",
          "smartrule.microservice.url": "http://localhost:8181/service/smartrule",
          "sendDashboardAgent.url": "http://localhost:19191/report",
          "mongodb.user": "c8y-root",
          "mongodb.admindb": "admin",
          "mongodb.password": "mongo123",
          "contextService.rdbmsURL": "jdbc:postgresql://localhost",
          "contextService.rdbmsDriver": "org.postgresql.Driver",
          "contextService.rdbmsUser": "postgres",
          "contextService.tenantManagementDB": "management",
          "cumulocity.environment": "PRODUCTION",
          "auth.checkBlockingFromOutside": "false",
          "migration.tomongo.default": "MONGO_READ_WRITE_POSTGRES_WRITE",
          "smsGateway.host": "http://localhost:8181/service/messaging",
          "email.host": "postfix.cumulocity-staging7-nonprod.svc.cluster.local",
          "email.from": "no-reply@app.domain.com",
          "system.two-factor-authentication.enabled": true,
          "system.two-factor-authentication.max.inactive": "10",
          "system.two-factor-authentication.enforced": "ashutosh",
          "system.two-factor-authentication.enforced.group": "ashutoshTfaTest",
          "errorMessageRepresentationBuilder.includeDebug": "false",
          "default.tenant.applications": "administration,devicemanagement,cockpit,feature-microservice-hosting,feature-cep-custom-rules",
          "passwordReset.email.subject": "Password reset",
          "passwordReset.token.email.template": "Dear app.domain.com user,\\n\\n\\\n            You or someone else entered this email address when trying to change the password of a app.domain.com portal user.\\n\\n\\\n            Please use the following link to reset your password: \\n\\\n            {host}?token={token}&showTenant\\n\\n\\\n            If you were not expecting this email, please ignore it. \\n\\n\\\n            Kind regards,\\n\\\n            app.domain.com support team\\n",
          "passwordReset.user.not.found.email.template": "Hi there,\\n\\n\\\n            you or someone else entered this email address when trying to change the password of a app.domain.com portal user.\\n\\n\\\n            However, we could not find the email address in your account. Please contact the administrator of your \\\n            account to set your email address and password. If you are the administrator of the account,\\\n            please use the email address that you registered with.\\n\\n\\\n            If you were not expecting this email, please ignore it. In case of questions, please get in contact with the administrator of app.domain.com portal. \\n\\n\\\n            Kind regards,\\n\\\n            app.domain.com support team\\n",
          "passwordReset.success.email.template": "Dear app.domain.com user,\\n\\n\\\n            Your password on {host} has been recently changed. \\n\\\n            If you or your administrator made this change, you do not need to do anything more. \\n\\\n            If you did not make this change, please contact your administrator.\\n\\n\\\n            Kind regards,\\n\\\n            app.domain.com support team\\n",
          "passwordReset.invite.template": "Hi there,\\n\\n\\\n            Please use the following link to reset your password: \\n\\\n            {host}/apps/devicemanagement/index.html?token={token}\\n\\n\\\n            If you were not expecting this email, please ignore it. \\n\\n\\\n            Kind regards,\\n\\n\\\n            app.domain.com support team\\n"
        }
      },
      "cumulocity-external-lb": {
        "landing_page": "https://acme.com/apps/devicemanagement",
        "paas_default_page": "https://$http_host/apps/$defapp",
        "paas_public_default_page": "https://devel-jenkins.c8y.io/apps/dmpublic",

        "usePostgresForPaaS": false,
        "paas_redirection": true,
        "temp_chunkin": false,
        "useIPAddress": true,
        "useLUAforHealthCheck": true,
        "useLUAforLimits": true,
        "useLUAforSSLcerts": true,
        "useHSTS": false,
        "useMQTTsupport": true,
        "useMasterForPushOperations": false,
        "useKarafWebsocket": true,
        "useNonIPhashPorts": true,
        "proxy_cache": false,
        "certificate_domain": "acme.com",
        "nginx": {
          "NGinxPort": "openresty",
          "version": "1.13.6.2-20.el7.centos.c8y.1006.7.0"
        }
      },
      "vendme-platform-agent": {
        "use-internal-proxy": null,
        "install-agent": null,
        "install-platform": null,
        "install-tracker": null
      },
      "cumulocity-rsyslog": {
        "cross-env-log-server": "cumulocity-multinode-prod",
        "log-server-ext-address": "monitoring.cumulocity.com"
      }
    }
  }' > environments/$environment_name.json

knife environment from file environments/$environment_name.json