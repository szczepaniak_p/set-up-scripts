#!/bin/bash
mkdir -p ~/cumulocity-chef/.chef/{access_certs,keys,environments,provisioning}  && cd cumulocity-chef

echo "
source 'https://rubygems.org'

gem 'chef', '12.20.3'
gem 'knife-ec2', '0.19.10'
gem 'chef-provisioning', '2.7.2'
gem 'aws-eventstream','1.0.3'
gem 'chef-vault','3.4.2'
gem 'aws-sdk','2.11.271'
gem 'aws-sdk-core','2.11.271'
gem 'aws-sdk-resources','2.11.271'
gem 'aws-sigv4','1.1.0'
gem 'chef-provisioning-aws', '3.0.7'
gem 'cheffish','13.1.0'
gem 'fog-aws','2.0.1'
gem 'ffi','1.13.1'
gem 'rubyzip','1.3.0'
" > Gemfile

yum install -y curl git openssh-client
gpg2 --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
curl -sSL https://get.rvm.io | bash -s stable
echo 'source /etc/profile.d/rvm.sh' >> ~/.bashrc && source ~/.bashrc  && rvm reload
rvm install 2.3.6
gem install bundler --version "=1.17.3" &&  bundle install


